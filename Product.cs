﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SQLite.Console
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        public string Description { get; set; }
    }
}
