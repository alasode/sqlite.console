using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SQLite.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
            AddProductUsingAdo("Glue");
            DisplayProductsUsingAdo();
            AddProductUsingAdo("Bulbs");
            DisplayProductsUsingEF();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        #region ADO.Net

        private static bool AddProductUsingAdo(string ProductName)
        {
            SqliteConnection conn = new SqliteConnection(@"Data Source=DB\ChampionProductsDB.db;");
            conn.Open();

            SqliteCommand command;

            using (conn)
            {
                command = conn.CreateCommand();
                command.CommandText = "Insert into product (Description) Values ('" + ProductName + "')";
                command.ExecuteNonQuery();
            }

            return true;

        }
        private static void DisplayProductsUsingAdo()
        {
            SqliteConnection conn = new SqliteConnection(@"Data Source = DB\ChampionProductsDB.db;");
            conn.Open();

            SqliteDataReader datareader;
            SqliteCommand command;

            using (conn)
            {
                command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Product";

                datareader = command.ExecuteReader();
                while (datareader.Read())
                {
                    string product = datareader.GetString(1);
                    Debug.WriteLine(product);
                }

            }
        }
        #endregion

        #region EF
        public static void AddProductUsingEF(string ProductName)
        {
            using var db = new ChampionContext();
            var newProduct = new Product()
            {
                Description = ProductName
            };

            db.Products.Add(newProduct);
            db.SaveChanges();
        }

        public static void DisplayProductsUsingEF()
        {
            using var db = new ChampionContext();
            var products = db
                            .Products
                            .AsNoTracking();

            foreach (var item in products)
            {
                Debug.WriteLine(item.Description);
            }
        }
        #endregion
    }
}
